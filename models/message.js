var mongoose=require('mongoose');
var Schema=mongoose.Schema;
var MessageSchema= new Schema(
    {
        title: {type: String, required: true},
        author:{ type: Schema.Types.ObjectId, ref: 'Author', required:true},
        summary: {type: String, required: true},
        priority:{types:Boolean},
        category:[{ type: Schema.Types.ObjectId, ref: 'Category'}]
    },
);

//virtual mess URL
MessageSchema.virtual('url').get(function(){
    return '/catalog/message/'+ this._id;
});

module.exports=mongoose.model('Message', MessageSchema);