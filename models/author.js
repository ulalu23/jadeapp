var mongoose=require('mongoose');

var Schema=mongoose.Schema;
var authorSchema=new Schema(
    {
        name:{type:String, required:true, max:100},
        position:{type:String, required: true, max:100},
        contact_nr:{type:Number, required:true, max:20 },
        social_media:{type:String,max:100}
    }
);
//virtual for author full name
authorSchema.virtual('name_work').get(function(){
    return this.name +','+ this.position;
});

//virtual full contact info
authorSchema.virtual('contact_info').get(function(){
    return this.contact_nr+' , '+this.social_media;
});
//URL virtual author
authorSchema.virtual('url').get(function(){
    return '/catalog/author'+ this._id;
});

module.exports=mongoose.model('Author',authorSchema);