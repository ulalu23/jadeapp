#! /usr/bin/env node

console.log('e.g.: populatedb mongodb+srv://cooluser:coolpassword@cluster0-mbdj7.mongodb.net/local_library?retryWrites=true');

// Get arguments passed on command line
var userArgs = process.argv.slice(2);
/*
if (!userArgs[0].startsWith('mongodb')) {
    console.log('ERROR: You need to specify a valid mongodb URL as the first argument');
    return
}
*/
var async = require('async')
var Message = require('./models/message')
var Author = require('./models/author')
var Category = require('./models/category')



var mongoose = require('mongoose');
var mongoDB = userArgs[0];
mongoose.connect(mongoDB, { useNewUrlParser: true });
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

var authors = []
var categories = []
var messages = []


function authorCreate(name, position,contact, socialm, cb) {
    authordetail = {name:name , position:position }
    if (contact != false) authordetail.contact_nr = contact
    if (socialm != false) authordetail.social_media= socialm

    var author = new Author(authordetail);

    author.save(function (err) {
        if (err) {
            cb(err, null)
            return
        }
        console.log('New Author: ' + author);
        authors.push(author)
        cb(null, author)
    }  );
}

function categoryCreate(name, cb) {
    var category= new Category({ name: name });

    category.save(function (err) {
        if (err) {
            cb(err, null);
            return;
        }
        console.log('New Category: ' + cat);
        categories.push(cat)
        cb(null, cat);
    }   );
}

function messageCreate(title, summary,priority, author, category, cb) {
    messdetail = {
        title: title,
        summary: summary,
        author: author,
        priority: priority,
    }
    if (category != false) messdetail.category = category;

    var mess = new Message(messdetail);
    mess.save(function (err) {
        if (err) {
            cb(err, null)
            return
        }
        console.log('New messages: ' + mess);
        messages.push(mess)
        cb(null, mess)
    }  );
}





function createCatAuthors(cb) {
    async.series([
            function(callback) {
                authorCreate('Patrick kowal', 'OPZZ Centrala member', '567234567', false, callback);
            },
            function(callback) {
                authorCreate('Ben stone', 'OPZZ exBoard', '193118345', false, callback);
            },
            function(callback) {
                authorCreate('Isaac Asimov', 'OPZZ Opole member', '345674576', 'skype lol', callback);
            },
            function(callback) {
                authorCreate('Bob siks', 'OPZZ Wroclaw member', 234543234, false, callback);
            },
            function(callback) {
                authorCreate('Jim Lordy', 'IP bytom', '234646788', 'fb tt', callback);
            },
            function(callback) {
                categoryCreate("Information", callback);
            },
            function(callback) {
                categoryCreate("Event", callback);
            },
            function(callback) {
                categoryCreate("Action", callback);
            },
        ],
        // optional callback
        cb);
}


function createMessages(cb) {
    async.parallel([
            function(callback) {
                messageCreate('The Name of', 'I ha barrow kings. I burned down the town of Trebon. age than most people are allowed in. I tread paths by he minstrels weep.', true, authors[0], [categories[0],], callback);
            },
            function(callback) {
                messageCreate("The Wise #2)", 'Picking up the tale of, we follow him int mightiestthe unassuming pub landlord.', false, authors[0], [messages[0],], callback);
            },
            function(callback) {
                messageCreate("The Slow Kingkiller Chronicle", 'Deep below the University,ooms. art of this forgotten place.', false, authors[2], [messages[1],], callback);
            },
            function(callback) {
                messageCreate("Apes and Angels", "Humankind,  Humans went to the stars  A wave o galaxy, an expanding sphere of lethal gamma ...", true, authors[1], [messages[1],], callback);
            },
            function(callback) {
                messageCreate("Death Wave","Iell led the solart revealed to Jordan Kell thae acts to save itself, all life on Earth will be wiped out...", false, authors[0], [messages[1],], callback);
            },
            function(callback) {
                messageCreate('Test Book 1', 'Summary of test book 1', false, authors[4], [messages[0]], callback);
            },
            function(callback) {
                messageCreate('Test Book 2', 'Summary of test book 2', false, authors[4], [messages[3]], callback)
            }
        ],
        // optional callback
        cb);
}






async.series([
        createCatAuthors,
        createMessages,
    ],
// Optional callback
    function(err, results) {
        if (err) {
            console.log('FINAL ERR: '+err);
        }
        else
        // All done, disconnect from database
        mongoose.connection.close();
    });