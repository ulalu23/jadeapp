var express = require('express');
var router = express.Router();


var mess_controller = require('../controllers/messageController');
var author_controller = require('../controllers/authorController');
var cat_controller = require('../controllers/categoryController');



/// BOOK ROUTES ///

// GET catalog home page.
router.get('/', mess_controller.index);

// GET request for creating a Book. NOTE This must come before routes that display Book (uses id).
router.get('/message/create', mess_controller.mess_create_get);

// POST request for creating Book.
router.post('/message/create', mess_controller.mess_create_post);

// GET request to delete Book.
router.get('/message/:id/delete', mess_controller.mess_delete_get);

// POST request to delete Book.
router.post('/message/:id/delete', mess_controller.mess_delete_post);

// GET request to update Book.
router.get('/message/:id/update', mess_controller.mess_update_get);

// POST request to update Book.
router.post('/message/:id/update', mess_controller.mess_update_post);

// GET request for one Book.
router.get('/message/:id', mess_controller.mess_detail);

// GET request for list of all Book.
router.get('/messages', mess_controller.mess_list);

/// AUTHOR ROUTES ///

// GET request for creating Author. NOTE This must come before route for id (i.e. display author).
router.get('/author/create', author_controller.author_create_get);

// POST request for creating Author.
router.post('/author/create', author_controller.author_create_post);

// GET request to delete Author.
router.get('/author/:id/delete', author_controller.author_delete_get);

// POST request to delete Author
router.post('/author/:id/delete', author_controller.author_delete_post);

// GET request to update Author.
router.get('/author/:id/update', author_controller.author_update_get);

// POST request to update Author.
router.post('/author/:id/update', author_controller.author_update_post);

// GET request for one Author.
router.get('/author/:id', author_controller.author_detail);

// GET request for list of all Authors.
router.get('/authors', author_controller.author_list);


/// GENRE ROUTES ///

// GET request for creating a Genre. NOTE This must come before route that displays Genre (uses id).
router.get('/category/create', cat_controller.cat_create_get);

// POST request for creating Genre.
router.post('/category/create', cat_controller.cat_create_post);

// GET request to delete Genre.
router.get('/category/:id/delete', cat_controller.cat_delete_get);

// POST request to delete Genre.
router.post('/category/:id/delete', cat_controller.cat_delete_post);

// GET request to update Genre.
router.get('/category/:id/update', cat_controller.cat_update_get);

// POST request to update Genre.
router.post('/category/:id/update', cat_controller.cat_update_post);

// GET request for one Genre.
router.get('/category/:id', cat_controller.cat_detail);

// GET request for list of all Genre.
router.get('/categories', cat_controller.cat_list);





module.exports = router;