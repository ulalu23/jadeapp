var Message = require('../models/message');
var Author=require('../models/author');
var Category=require('../models/category');

var async=require('async');

exports.index = function(req, res) {
   async.parallel({
       mess_count: function(callback){
           Message.count({}, callback)
       },
       author_count: function (callback){
           Author.count({}, callback)
       },
       cat_count: function(callback){
           Category.count({}, callback)
    }
   }, function (err,results) {
       res.render('index',{ title: 'MESSAGE BOARD', error: err, data: results})
   })
};

// Display list of all books.
exports.mess_list = function(req, res, next) {
    Message.find({}, 'title_author')
        .populate('author')
        .exec(function(err, list_messes){
        if (err) { return next(err);}
    res.render('mess_list', {title:'Messages List', mess_list: list_messes});
    });
};

// Display detail page for a specific book.
exports.mess_detail = function(req, res) {
    res.send('NOT IMPLEMENTED: Book detail: ' + req.params.id);
};

// Display book create form on GET.
exports.mess_create_get = function(req, res) {
    res.send('NOT IMPLEMENTED: Book create GET');
};

// Handle book create on POST.
exports.mess_create_post = function(req, res) {
    res.send('NOT IMPLEMENTED: Book create POST');
};

// Display book delete form on GET.
exports.mess_delete_get = function(req, res) {
    res.send('NOT IMPLEMENTED: Book delete GET');
};

// Handle book delete on POST.
exports.mess_delete_post = function(req, res) {
    res.send('NOT IMPLEMENTED: Book delete POST');
};

// Display book update form on GET.
exports.mess_update_get = function(req, res) {
    res.send('NOT IMPLEMENTED: Book update GET');
};

// Handle book update on POST.
exports.mess_update_post = function(req, res) {
    res.send('NOT IMPLEMENTED: Book update POST');
};