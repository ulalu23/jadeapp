var Category = require('../models/category');

var Message = require('../models/message');
var async = require('async');

// Display list of all Genre.
exports.cat_list = function(req, res,next) {
    Category.find().sort([['name','ascending']]).exec( function(err, list_categories){
        if(err) { return next(err); }
        res.render('cat_list',{title: 'Categories List',cat_list:list_categories})
    });
};

// Display detail page for a specific CATEGORIES
exports.cat_detail = function(req, res,next) {

    async.parallel({
        category: function(callback){
            Category.findById(req.params.id).exec(callback)
        },
        category_messages:function(callback){
            Message.find({ 'category': req.params.id}).exec(callback)
        },
    }, function(err,results){
        if (err) {return next(err); }
        if (results.category==null) {
            var err = new Error('Category not found');
            err.status = 404;
            return next(err);
        }
        res.render( 'cat_detail', { title: "Category details", category: results.category, category_messages: results.category_messages} );
    });
};

// Display Genre create form on GET.
exports.cat_create_get = function(req, res) {
    res.send('NOT IMPLEMENTED: Genre create GET');
};

// Handle Genre create on POST.
exports.cat_create_post = function(req, res) {
    res.send('NOT IMPLEMENTED: Genre create POST');
};

// Display Genre delete form on GET.
exports.cat_delete_get = function(req, res) {
    res.send('NOT IMPLEMENTED: Genre delete GET');
};

// Handle Genre delete on POST.
exports.cat_delete_post = function(req, res) {
    res.send('NOT IMPLEMENTED: Genre delete POST');
};

// Display Genre update form on GET.
exports.cat_update_get = function(req, res) {
    res.send('NOT IMPLEMENTED: Genre update GET');
};

// Handle Genre update on POST.
exports.cat_update_post = function(req, res) {
    res.send('NOT IMPLEMENTED: Genre update POST');
};